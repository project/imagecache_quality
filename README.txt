You *MUST* patch the following modules with the patches supplied in order
for Imagecache Quality module to work.

These patches are compatible with:

  Imageapi: 6.x-10
  Imagecache: 6.x-2.0-beta12
  EPSA crop: 6.x-1.2

When applying the patches, place them in the *parent root* of these modules.
For example, if you have these modules under:

  /sites/all/modules/contrib/imageapi
  /sites/all/modules/contrib/imagecache

... then place the patches in:

  /sites/all/modules/contrib

Then run the following command:

patch -p1 < [patch name]
